package com.desafio.projeto.controller;

import com.desafio.projeto.model.Funcao;
import com.desafio.projeto.model.UsuarioExterno;
import com.desafio.projeto.repository.UsuarioExternoRepository;

/**
 * Utility class for testing REST controllers.
 */

public class TestUtil {
	private static final String DEFAULT_CPF = "AAAAAAAAAA";
	private static final String UPDATED_CPF = "BBBBBBBBBB";
	
    private static final String DEFAULT_NOME = "AAAAAAAAAA";
    private static final String UPDATED_NOME = "BBBBBBBBBB";
    
    private static final String DEFAULT_SITUACAO = "A";
    private static final String UPDATED_SITUACAO = "B";
    
    private static final int DEFAULT_PERFIL = 1;
    private static final int UPDATED_PERFIL = 2;
    
    private static final Funcao DEFAULT_FUNCAO = new Funcao();
    
    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_TELEFONE = "AAAAAAAAAA";
    private static final String UPDATED_TELEFONE = "BBBBBBBBBB";
    
    private static UsuarioExternoRepository usuarioRepository;


	public static UsuarioExterno createUsuario() {
		UsuarioExterno usuario = new UsuarioExterno();
		
		usuario.setNo_usuario(TestUtil.DEFAULT_NOME);
		usuario.setNu_cpf(TestUtil.DEFAULT_CPF);
		usuario.setDe_email(TestUtil.DEFAULT_EMAIL);
		usuario.setIc_situacao(TestUtil.DEFAULT_SITUACAO);
		usuario.setIc_perfil_acesso(TestUtil.DEFAULT_PERFIL);
		usuario.setCo_funcao(TestUtil.DEFAULT_FUNCAO);
		usuario.setNu_telefone(TestUtil.DEFAULT_TELEFONE);
		
		return usuario;
	}
	
	public static UsuarioExterno updateUsuario() {
		UsuarioExterno usuario = usuarioRepository.findById(1L).get();
		
		usuario.setNo_usuario(TestUtil.UPDATED_NOME);
		usuario.setNu_cpf(TestUtil.UPDATED_CPF);
		usuario.setDe_email(TestUtil.UPDATED_EMAIL);
		usuario.setIc_situacao(TestUtil.UPDATED_SITUACAO);
		usuario.setIc_perfil_acesso(TestUtil.UPDATED_PERFIL);
		usuario.setNu_telefone(TestUtil.UPDATED_TELEFONE);
		UsuarioExterno usuarioAutalizado = usuarioRepository.save(usuario);
		
		return usuarioAutalizado;
	}
}
