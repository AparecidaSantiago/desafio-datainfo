package com.desafio.projeto.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import com.desafio.projeto.controller.TestUtil;

@RunWith(SpringRunner.class)
public class UsuarioExternoTest {
private UsuarioExterno usuario;
	
	@Before
	public void setup() {
		usuario = TestUtil.createUsuario();
	}
	
	@Test
	public void getIdTest() {
		Assert.assertNotNull("id não pode ser nulo", usuario.getId());
	}
	
	@Test
	public void getNomeTest() {
		Assert.assertNotNull("nome não pode ser nulo", usuario.getNo_usuario());
	}
	
	@Test
	public void getCpfTest() {
		Assert.assertNotNull("Cpf não pode ser nulo", usuario.getNu_cpf());
	}
	
	@Test
	public void getFuncaoTest() {
		Assert.assertNotNull("Função não pode ser nulo", usuario.getCo_funcao());
	}
}
