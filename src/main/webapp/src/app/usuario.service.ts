import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Usuario } from './usuario';
import { MensagemService } from './mensagem.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  private usuariosUrl = 'http://localhost:8080/api/usuarios';

  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  constructor(private http: HttpClient, private mensagemService: MensagemService) { }

  getUsuarios(): Observable<Usuario[]> {
    return this.http.get<Usuario[]>(this.usuariosUrl).pipe(
        tap(_ => this.log('fetched usuários')),
        catchError(this.tratarErro<Usuario[]>('getUsuarios', []))
    );
  }

  getUsuario(id: number): Observable<Usuario> {
    const url = `${this.usuariosUrl}/${id}`;

    return this.http.get<Usuario>(`${this.usuariosUrl}/${id}`).pipe(
        tap(_ => this.log(`fetched usuário id=${id}`)),
        catchError(this.tratarErro<Usuario>(`getUsuariosid=${id}`))
    );
  }

  postUsuario(usuario: Usuario): Observable<Usuario> {
    return this.http.post<Usuario>(this.usuariosUrl, usuario, this.httpOptions).pipe(
      tap((newUsuario: Usuario) => this.log(`added usuario w/ id=${newUsuario.id}`)),
      catchError(this.tratarErro<Usuario>('postUsuario'))
    );
  }

  putUsuario(usuario: Usuario): Observable<any> {
    const url = `${this.usuariosUrl}/${usuario.id}`;

    return this.http.put(url, usuario, this.httpOptions).pipe(
      tap(_ => this.log(`updated usuario id=${usuario.id}`)),
      catchError(this.tratarErro<any>('putUsuario'))
    );
  }

  /**
   * Trata uma operação Http que falhou.
   * @param operation - nome da operação que falhou
   * @param result - valor opcional para ser retornado como resultado do observable
  */
  private tratarErro<T>(operacao, result?: T) {
    return (error: any): Observable<T> => {
      this.log(`${operacao} falhou: ${error.message}`);
      return of(result as T);
    };
  }

  private log(message: string) {
    this.mensagemService.add(`UsuarioService: ${message}`);
  }
}
