import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import { Usuario } from '../usuario';
import { UsuarioService } from '../usuario.service';

@Component({
  selector: 'app-usuario-detalhes',
  templateUrl: './usuario-detalhes.component.html',
  styleUrls: ['./usuario-detalhes.component.css']
})
export class UsuarioDetalhesComponent implements OnInit {
  @Input() usuario: Usuario;
  cadastro: boolean = false;

  formUsuario = new FormGroup({
    email: new FormControl('', [
      Validators.required,
      Validators.email,
    ]),

    nome: new FormControl('', [Validators.required]),

    cpf: new FormControl('', [
      Validators.required,
      Validators.pattern(/^(\d{3}\.){2}\d{3}\-\d{2}$/),
    ]),

    telefone: new FormControl(),
    funcao: new FormControl('', [ Validators.required]),
    perfil: new FormControl('', [Validators.required]),
  });

  constructor(
    private route: ActivatedRoute,
    private usuarioService: UsuarioService,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.getUsuario();
  }

  getUsuario(): void {
    const id = +this.route.snapshot.paramMap.get('id');

    if (id) {
      this.usuarioService.getUsuario(id).subscribe(usuario => {
        this.usuario = usuario;
        this.formUsuario.controls.email.setValue(usuario.email);
        this.formUsuario.controls.nome.setValue(usuario.nome);
        this.formUsuario.controls.cpf.setValue(usuario.cpf);
        this.formUsuario.controls.telefone.setValue(usuario.telefone);
        this.formUsuario.controls.funcao.setValue(usuario.funcao);
        this.formUsuario.controls.perfil.setValue(usuario.perfil);
      });
    }
    else {
      this.cadastro = true;
      this.usuario = <Usuario>{};
    }
  }

  voltar(): void {
    this.location.back();
  }

  cadastrarUsuario(): void {
    this.usuarioService.postUsuario(this.usuario).subscribe(() => this.voltar());
  }

  atualizarUsuario(): void {
    this.usuarioService.putUsuario(this.usuario).subscribe(() => this.voltar());
  }
}
