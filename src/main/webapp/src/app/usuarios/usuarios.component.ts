import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { tap } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';

import { Usuario, RespostaOperacao } from '../model/usuario';
import { UsuarioService } from '../services/usuario.service';
import { UsuariosDataSource } from "../services/usuarios.datasource";
import { ConfirmDialogModel, ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css']
})
export class UsuariosComponent implements OnInit {
  usuariosDataSource: UsuariosDataSource;
  camposFiltro = <any>{};
  filtro = <any>{};
  colunas: string[] = ['email', 'nome', 'perfil', 'ativo', 'acoes'];
  Perfis: any = {1: 'Gestor Municipal', 2: 'Gestor Estadual', 3: 'Gestor Nacional'};

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor(
    private usuarioService: UsuarioService,
    private snackBar: MatSnackBar,
    public dialog: MatDialog) {}

  ngOnInit(): void {
    this.usuariosDataSource = new UsuariosDataSource(this.usuarioService);

    this.filtro = <any>{
      pagina: 0,
      limite: 5
    };

    this.usuariosDataSource.loadUsuarios(this.filtro);
    this.getPaginacao();
  }

  ngAfterViewInit() {
    this.paginator.page.pipe(tap(() => this.loadPaginaUsuarios())).subscribe();
  }
 
  loadPaginaUsuarios() {
    this.filtrarUsuarios(false);
    this.usuariosDataSource.loadUsuarios(this.filtro);
  }

  filtrarUsuarios(resetPagina=true) {
    if (resetPagina)
      this.paginator.pageIndex = 0;
      
    this.filtro = <any>{
      pagina: this.paginator.pageIndex,
      limite: this.paginator.pageSize
    };

    if (this.camposFiltro.nome)
      this.filtro.nome = this.camposFiltro.nome;
    if (this.camposFiltro.situacao)
      this.filtro.situacao = this.camposFiltro.situacao;
    if (this.camposFiltro.perfil)
      this.filtro.perfil = this.camposFiltro.perfil;
    
    if (resetPagina)
      this.loadPaginaUsuarios();
  }

  deletarUsuario(usuario: Usuario): void {
    this.dialog.open(ConfirmDialogComponent, {
      maxWidth: "500px",
      data: new ConfirmDialogModel("Excluir Usuário", `Deseja realmente excluir o usuário ${usuario.no_usuario}?`)
    })
    .afterClosed().subscribe(result => {
      if (result) {
        this.usuariosDataSource.deleteUsuario(usuario).subscribe((resp: RespostaOperacao) => {
          this.snackBar.open(resp.message, resp.success ? "Sucesso" : "Falha", {duration: 3000, horizontalPosition: 'center', verticalPosition: 'top'});
          this.loadPaginaUsuarios();
          this.getPaginacao();
        });
      }
    });
  }

  setSituacao(usuario: Usuario): void {
    usuario.ic_situacao = usuario.ic_situacao == "A" ? "I" : "A";

    this.usuarioService.setSituacao(usuario).subscribe(resp => {
      this.snackBar.open(resp.message, resp.success ? "Sucesso" : "Falha", {duration: 3000, horizontalPosition: 'center', verticalPosition: 'top'});
      if (!resp.success)
        usuario.ic_situacao = usuario.ic_situacao == "A" ? "I" : "A";
    });
  }

  getPaginacao() {
    this.usuarioService.getPaginacao().subscribe(resp => {
      this.paginator.length = resp.qtdTotal;
    });
  }
}
