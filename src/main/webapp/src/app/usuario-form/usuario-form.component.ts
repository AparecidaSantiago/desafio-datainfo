import { Component, OnInit, Input, ViewChild, } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { FormGroup, FormBuilder, Validators, NgForm } from "@angular/forms";
import { MatSnackBar } from '@angular/material/snack-bar';

import { Usuario, Funcao } from '../model/usuario';
import { UsuarioService } from '../services/usuario.service';

@Component({
  selector: 'app-usuario-form',
  templateUrl: './usuario-form.component.html',
  styleUrls: ['./usuario-form.component.css']
})
export class UsuarioFormComponent implements OnInit {
  @Input() usuario: Usuario;
  cadastro: boolean = false;
  formUsuario: FormGroup;
  Perfis: any = {1: 'Gestor Municipal', 2: 'Gestor Estadual', 3: 'Gestor Nacional'};
  funcoes: Funcao[];

  @ViewChild('formDirective') private formDirective: NgForm;

  constructor(
    private usuarioService: UsuarioService,
    private route: ActivatedRoute,
    private router: Router,
    public fb: FormBuilder,
    private location: Location,
    private snackBar: MatSnackBar
  ) {
    if (this.router.getCurrentNavigation().extras.state)
      this.usuario = this.router.getCurrentNavigation().extras.state.usuario;
  }

  ngOnInit(): void {
    const id = +this.route.snapshot.paramMap.get('id');

    if (id) {
      if (!this.usuario) {
        this.router.navigateByUrl('/usuarios');
      }
      else {
        console.log(this.usuario);
        this.setFormUsuario();
      }  
    }
    else {
      this.cadastro = true;
      this.usuario = <Usuario>{};
      this.setFormUsuario();
    }

    this.getFuncoes();
  }
  
  //seta os validadores e os dados no form
  setFormUsuario(): void {
    this.formUsuario = this.fb.group({
      de_email: [this.usuario.de_email, [
        Validators.required,
        Validators.email,
        Validators.max(255)
      ]],
      no_usuario: [this.usuario.no_usuario, [
        Validators.required,
        Validators.max(50)
      ]],
      nu_cpf: [this.usuario.nu_cpf, [
        Validators.required,
        Validators.min(11),
      ]],
      nu_telefone: [this.usuario.nu_telefone, [
        Validators.pattern(/^[0-9]\d*$/)
      ]],
      co_funcao: [this.usuario.co_funcao, [Validators.required]],
      ic_perfil_acesso: [this.usuario.ic_perfil_acesso, [Validators.required]]
    });
  }

  voltar(): void {
    this.location.back();
  }

  cadastrarUsuario(): void {
    if (this.formUsuario.valid) {
      const usuario = {...this.formUsuario.value, ic_situacao: "A"};
      this.usuarioService.postUsuario(usuario).subscribe(resp => {
        this.snackBar.open(resp.message, resp.success ? "Sucesso" : "Falha", {duration: 3000, horizontalPosition: 'center', verticalPosition: 'top'});

        if (resp.success){
          this.formUsuario.reset();
          this.formDirective.resetForm();
        }
      });
    }
  }

  atualizarUsuario(): void {
    if (this.formUsuario.valid) {
      const usuario = {...this.usuario, ...this.formUsuario.value};
      this.usuarioService.putUsuario(usuario).subscribe(resp => {
        this.snackBar.open(resp.message, resp.success ? "Sucesso" : "Falha", {duration: 3000, horizontalPosition: 'center', verticalPosition: 'top'});
      });
    }
  }

  getFuncoes() {
    this.usuarioService.getFuncoes().subscribe(funcoes => this.funcoes = funcoes);
  }

  //trata campos com valores inválidos no form
  public handleError = (controlName: string, errorName: string) => {
    return this.formUsuario.controls[controlName].hasError(errorName);
  } 
}
