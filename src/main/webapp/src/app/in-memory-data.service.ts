import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Usuario } from './usuario';

@Injectable({
  providedIn: 'root',
})
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const usuarios = [
      {id: 1, cpf: '65944766603', nome: 'userA', email: 'userA@mail.com', situacao: 'A', perfil: 1, funcao: 4, telefone: '32314752', ativo: false},
      {id: 2, cpf: '12859417084', nome: 'userB', email: 'userB@mail.com', situacao: 'S', perfil: 1, funcao: 4, telefone: '80944484', ativo: true},
      {id: 3, cpf: '17484780888', nome: 'userC', email: 'userC@mail.com', situacao: 'S', perfil: 2, funcao: 4, telefone: '33333333', ativo: true},
      {id: 4, cpf: '08787848966', nome: 'userD', email: 'userD@mail.com', situacao: 'A', perfil: 1, funcao: 3, telefone: '425642624', ativo: false},
      {id: 5, cpf: '70170111444', nome: 'userE', email: 'userE@mail.com', situacao: 'S', perfil: 2, funcao: 3, telefone: '6234677777', ativo: false},
    ];

    return {usuarios};
  }

  genId(usuarios: Usuario[]): number {
    return usuarios.length > 0 ? Math.max(...usuarios.map(usuario => usuario.id)) + 1 : 1;
  }
}