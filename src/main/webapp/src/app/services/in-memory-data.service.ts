import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Usuario } from '../model/usuario';

@Injectable({
  providedIn: 'root',
})
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const usuarios = [
      {id: 1, nu_cpf: '65944766603', no_usuario: 'userA', de_email: 'userA@mail.com', ic_perfil_acesso: "1", co_funcao: "4", nu_telefone: '32314752', ic_situacao: "I"},
      {id: 2, nu_cpf: '12859417084', no_usuario: 'userB', de_email: 'userB@mail.com', ic_perfil_acesso: "3", co_funcao: "2", nu_telefone: '80944484', ic_situacao: "A"},
      {id: 3, nu_cpf: '17484780888', no_usuario: 'userC', de_email: 'userC@mail.com', ic_perfil_acesso: "2", co_funcao: "4", nu_telefone: '33333333', ic_situacao: "A"},
      {id: 4, nu_cpf: '08787848966', no_usuario: 'userD', de_email: 'userD@mail.com', ic_perfil_acesso: "1", co_funcao: "3", nu_telefone: '425642624', ic_situacao: "I"},
      {id: 5, nu_cpf: '70170111444', no_usuario: 'userE', de_email: 'userE@mail.com', ic_perfil_acesso: "2", co_funcao: "3", nu_telefone: '6234677777', ic_situacao: "I"},
    ];

    const funcoes = [
      {id: 1, descricao: 'Gestor'},
      {id: 2, descricao: 'Administrador'},
      {id: 3, descricao: 'Frente de Caixa'}
    ];

    return {usuarios, funcoes};
  }

  genId(usuarios: Usuario[]): number {
    return usuarios.length > 0 ? Math.max(...usuarios.map(usuario => usuario.id)) + 1 : 1;
  }
}