
import { CollectionViewer, DataSource } from "@angular/cdk/collections";
import { Observable, BehaviorSubject, of, forkJoin } from "rxjs";
import { Usuario } from "../model/usuario";
import { UsuarioService } from "./usuario.service";
import { catchError, tap } from "rxjs/operators";

export class UsuariosDataSource implements DataSource<Usuario> {

  private usuariosSubject = new BehaviorSubject<Usuario[]>([]);

  constructor(private usuarioService: UsuarioService) {}

  loadUsuarios(filtro: any) {

    this.usuarioService.getUsuarios(filtro)
      .pipe(
        catchError(() => of([])),
      )
      .subscribe(usuarios => this.usuariosSubject.next(usuarios));
  }

  deleteUsuario(usuario: Usuario) {
    const usuarios: Usuario[] = this.usuariosSubject.getValue();
  
    usuarios.forEach((item, index) => {
      if (item === usuario)
        usuarios.splice(index, 1);
    });

    return this.usuarioService.deleteUsuario(usuario)
      .pipe(
        catchError(() => of([])),
        tap(res => {
          this.usuariosSubject.next(usuarios);
        })
      );
  }

  setSituacao(usuario: Usuario) {
    const usuarios: Usuario[] = this.usuariosSubject.getValue();
  
    usuarios.forEach((item, index) => {
      if (item === usuario)
        usuarios.splice(index, 1);
    });

    return this.usuarioService.setSituacao(usuario)
    .pipe(
      catchError(() => of([])),
      tap(res => {
        this.usuariosSubject.next(usuarios);
      })
    );
  }

  connect(collectionViewer: CollectionViewer): Observable<Usuario[]> {
    console.log("Connecting data source");
    return this.usuariosSubject.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this.usuariosSubject.complete();
  }
}