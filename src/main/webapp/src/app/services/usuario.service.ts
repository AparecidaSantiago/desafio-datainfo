import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Usuario, UsuarioResposta, FuncaoResposta, UsuarioApi, Funcao, RespostaOperacao } from '../model/usuario';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  private usuariosUrl = 'http://ece2569ec328.ngrok.io/api';

  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  constructor(private http: HttpClient) { }

  getUsuarios(filtro: any): Observable<Usuario[]> {
    const url = `${this.usuariosUrl}/usuarios`;

    console.log(filtro)

    return this.http.get<UsuarioResposta>(url, {params: filtro}).pipe(
        map((res: UsuarioResposta) => res.dados),
        map((users: UsuarioApi[]) => users.map((user: UsuarioApi) => <Usuario>{
          ...user,
          ic_perfil_acesso: String(user.ic_perfil_acesso),
          co_funcao: user.co_funcao.co_funcao,
        })),
        tap(_ => console.log(_)),
        catchError(this.tratarErro<Usuario[]>('getUsuarios', []))
    );
  }

  getFuncoes(): Observable<Funcao[]> {
    const url = `${this.usuariosUrl}/funcoes`;
    return this.http.get<FuncaoResposta>(url).pipe(
        map((res: FuncaoResposta) => res.content),
        tap(_ => console.log(_)),
        catchError(this.tratarErro<Funcao[]>('getFuncoes', []))
    );
  }

  getPaginacao(): Observable<UsuarioResposta> {
    const url = `${this.usuariosUrl}/usuarios`;
    const params = <any>{limite: 1};

    return this.http.get<UsuarioResposta>(url, {params: params}).pipe(
        catchError(this.tratarErro<UsuarioResposta>('getUsuarios'))
    );
  }

  postUsuario(usuario: Usuario): Observable<RespostaOperacao> {
    const url = `${this.usuariosUrl}/usuarios`;
    return this.http.post<RespostaOperacao>(url, usuario, this.httpOptions).pipe(
      tap((res: RespostaOperacao) => console.log(res)),
      catchError(this.tratarErro<any>('postUsuario'))
    );
  }

  putUsuario(usuario: Usuario): Observable<RespostaOperacao> {
    const url = `${this.usuariosUrl}/usuarios/${usuario.id}`;
    return this.http.put(url, usuario, this.httpOptions).pipe(
      tap(_ => console.log(`usuário atualizado id=${usuario.id}`)),
      catchError(this.tratarErro<any>('putUsuario'))
    );
  }

  deleteUsuario(usuario: Usuario): Observable<RespostaOperacao> {
    const url = `${this.usuariosUrl}/usuarios/${usuario.id}`;
    return this.http.delete<RespostaOperacao>(url, this.httpOptions).pipe(
      tap(_ => console.log(`usuário deletado id=${usuario.id}`)),
      catchError(this.tratarErro<any>('deleteUsuario'))
    );
  }

  setSituacao(usuario: Usuario): Observable<any> {
    const url = `${this.usuariosUrl}/usuarios/${usuario.id}`;
    const patch = {ic_situacao: usuario.ic_situacao};

    console.log(usuario);

    return this.http.patch(url, patch, this.httpOptions).pipe(
      tap(_ => console.log(`situação alterada id=${usuario.id}`)),
      catchError(this.tratarErro<any>('setSituacao'))
    );
  }

  /**
   * Trata uma operação Http que falhou.
   * @param operation - nome da operação que falhou
   * @param result - valor opcional para ser retornado como resultado do observable
  */
  private tratarErro<T>(operacao, result?: T) {
    return (error: any): Observable<T> => {
      console.log(`${operacao} falhou: ${error.message}`);
      return of(result as T);
    };
  }
}
