export interface Usuario {
  id: number;
  nu_cpf: string;
  no_usuario: string;
  de_email: string;
  ic_perfil_acesso: string;
  co_funcao: number;
  nu_telefone: string;
  ic_situacao: string;
}

export interface UsuarioApi {
  id: number;
  nu_cpf: string;
  no_usuario: string;
  de_email: string;
  ic_perfil_acesso: number;
  co_funcao: Funcao;
  nu_telefone: string;
  ic_situacao: string;
}

export interface Funcao {
  co_funcao: number;
  nome: string;
}

export interface UsuarioResposta {
  dados: UsuarioApi[];
  pagina: number;
  limite: number;
  qtdTotal: number;
  qtdPaginas: number;
  ultimaPagina: boolean;
  primeiraPagina: boolean;
}

export interface FuncaoResposta {
  content: Funcao[];
}

export interface RespostaOperacao {
  success: boolean;
  message: string;
}