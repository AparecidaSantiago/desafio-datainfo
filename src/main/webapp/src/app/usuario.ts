export interface Usuario {
  id: number;
  cpf: string;
  nome: string;
  email: string;
  situacao: string;
  perfil: number;
  funcao: number;
  telefone: string;
}