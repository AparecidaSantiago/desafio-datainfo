package com.desafio.specification;
import java.text.MessageFormat;
import org.springframework.data.jpa.domain.Specification;
import com.desafio.projeto.model.UsuarioExterno;

public final class UsuarioExternoSpecifications {
	public static Specification<UsuarioExterno> nomeContem(String valor) {
	    return (root, query, builder) -> builder.like(root.get("no_usuario"), contem(valor));
	}

	public static Specification<UsuarioExterno> situacao(String situacao) {
	    return (root, query, builder) -> builder.equal(root.get("ic_situacao"), situacao);
	}
	
	public static Specification<UsuarioExterno> perfil(Integer perfil) {
        return (root, query, builder) -> builder.equal(root.get("ic_perfil_acesso"), perfil);
    }

	private static String contem(String expression) {
	    return MessageFormat.format("%{0}%", expression);
	}
}
