package com.desafio.projeto.utils;

import org.springframework.http.HttpStatus;

import com.desafio.projeto.exceptions.ApiExceptions;

public class AppUtils {
	public static void validatePageNumberAndSize(int page, int size) {
        if(page < 0) {
            throw new ApiExceptions(HttpStatus.BAD_REQUEST, "Número de páginas não pode ser menor que zero");
        }

        if(size < 0) {
            throw new ApiExceptions(HttpStatus.BAD_REQUEST, "O limite de número de pág. não pode ser menor que zero");
        }

        if(size > Constants.MAX_PAGE_SIZE) {
        	throw new ApiExceptions(HttpStatus.BAD_REQUEST, "O limite da página não deve ser maior que " + Constants.MAX_PAGE_SIZE);
        }
    }
}
