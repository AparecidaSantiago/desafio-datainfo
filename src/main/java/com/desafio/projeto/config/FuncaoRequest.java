package com.desafio.projeto.config;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

/**
 * Um DTO para a entidade Função.
 */
@Getter
@Setter
public class FuncaoRequest {
	@NotBlank
	@Size(max = 11)
	private Integer co_funcao;
	
	@NotBlank
	@Size(max = 50)
	private String nome;
}
