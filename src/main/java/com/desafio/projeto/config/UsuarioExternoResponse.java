package com.desafio.projeto.config;

import org.springframework.data.domain.ExampleMatcher;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UsuarioExternoResponse {
    private String nu_cpf;
    private String no_usuario;
    private String de_email;
    private String ic_situacao;
    private int ic_perfil_acesso;
    private String co_funcao;
    private String nu_telefone;
	
	public UsuarioExternoResponse(String cpf, String nome, String email, String situacao, int perfil, String funcao, String telefone) {
		this.nu_cpf = cpf;
		this.no_usuario = nome;
		this.de_email = email;
		this.ic_situacao = situacao;
		this.ic_perfil_acesso = perfil;
		this.co_funcao = funcao;
		this.nu_telefone = telefone;
	}
	
	public UsuarioExternoResponse(String nome,  String situacao, int perfil, ExampleMatcher matcher) {
		this.no_usuario = nome;
		this.ic_situacao = situacao;
		this.ic_perfil_acesso = perfil;
	}
}
