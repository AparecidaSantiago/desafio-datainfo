package com.desafio.projeto.config;

import lombok.Getter;
import lombok.Setter;

/**
 * Um DTO para a entidade UsuárioExterno.
 */
@Getter
@Setter
public class UsuarioExternoRequest {

	private String nu_cpf;
	
	private String no_usuario;
	
	private String de_email;

	private String ic_situacao;
	
	private int ic_perfil_acesso;
	
	private Long co_funcao;

	private String nu_telefone;
}
