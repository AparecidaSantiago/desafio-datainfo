package com.desafio.projeto.config;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PagedResponse<T> {
    private List<T> dados; // contem a lista com os dados dos usuários retornados na consulta
    private int pagina;
    private int limite;
    private long qtdTotal; // quantidade total de registros presentes no banco de dados
    private int qtdPaginas; // numero total de páginas
    private boolean ultimaPagina; //indica se a página passada como parâmetro é a última página
    private boolean primeiraPagina; //indica se a página passada como parâmetro é a primeira página

    public PagedResponse(){

    }

    public PagedResponse(List<T> dados, int pagina, int limite, long qtdTotal, int qtdPaginas, boolean ultimaPagina,
						boolean primeiraPagina) {
        setDados(dados);
        this.pagina = pagina;
        this.limite = limite;
        this.qtdTotal = qtdTotal;
        this.qtdPaginas = qtdPaginas;
        this.ultimaPagina = ultimaPagina;
        this.primeiraPagina = primeiraPagina;
    }

    public List<T> getDados() {
		return dados == null ? null : new ArrayList<>(dados);
    }

    public final void setDados(List<T> dados) {
		if (dados == null) {
			this.dados = null;
		} else {
			this.dados = Collections.unmodifiableList(dados);
		}
    }

    public boolean isUltimaPagina() {
        return ultimaPagina;
    }

    public void setUltimaPagina(boolean ultimaPagina) {
        this.ultimaPagina = ultimaPagina;
    }
    
    public boolean isPrimeiraPagina() {
        return primeiraPagina;
    }

    public void setPrimeiraPagina(boolean primeiraPagina) {
        this.primeiraPagina = primeiraPagina;
    }
}