package com.desafio.projeto.config;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FuncaoResponse {
	 private int co_funcao;
	 private String nome;
	 
	 public FuncaoResponse(Integer coFuncao, String nome) {
		 this.co_funcao = coFuncao;
		 this.nome = nome;
	 }
}
