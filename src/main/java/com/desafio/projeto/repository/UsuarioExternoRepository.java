package com.desafio.projeto.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import com.desafio.projeto.model.UsuarioExterno;

/**
 * Spring Data JPA repository para a entidade UsuarioExterno.
 */
public interface UsuarioExternoRepository extends JpaRepository<UsuarioExterno, Long>, JpaSpecificationExecutor<UsuarioExterno> {
	UsuarioExterno findOneById(Long id);
	
	@Query("SELECT usuario FROM UsuarioExterno AS usuario WHERE nu_cpf LIKE ?1")
	UsuarioExterno findByCpf(String cpf);

}
