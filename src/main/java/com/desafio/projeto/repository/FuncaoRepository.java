package com.desafio.projeto.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.desafio.projeto.model.Funcao;

/**
 * Spring Data JPA repository para a entidade Funcao.
 */
public interface FuncaoRepository extends JpaRepository<Funcao, Long>, JpaSpecificationExecutor<Funcao> {

}
