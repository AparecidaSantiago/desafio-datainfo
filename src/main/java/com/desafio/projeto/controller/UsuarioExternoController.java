package com.desafio.projeto.controller;

import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.desafio.projeto.config.ApiResponse;
import com.desafio.projeto.config.PagedResponse;
import com.desafio.projeto.config.UsuarioExternoRequest;
import com.desafio.projeto.model.Funcao;
import com.desafio.projeto.model.UsuarioExterno;
import com.desafio.projeto.repository.FuncaoRepository;
import com.desafio.projeto.service.UsuarioExternoService;
import com.desafio.projeto.utils.Constants;

@CrossOrigin
@RestController
@RequestMapping("api/")
public class UsuarioExternoController {
private final Logger log = LoggerFactory.getLogger(UsuarioExternoService.class);
	
	@Autowired
	private UsuarioExternoService usuarioService;
	
	@Autowired
	private FuncaoRepository funcaoRepository;
	
	/**
     * GET /usuarios/: recupera todos os usuários por filtros. Se nenhum filtro for passado, recupera todos os usuários.
     *
     * @param none, nome, situacao, perfil
     *            os parâmetros para recuperar o(s) UsuarioExterno(s)
     * @return o ResponseEntity com status 200 (OK) e com o UsuarioExterno no body.
     */
	@GetMapping("/usuarios")
	public ResponseEntity<PagedResponse<UsuarioExterno>> getAllUsuariosByFiltro(@RequestParam Map<String, String> params,
		@RequestParam(value = "pagina", required = false, defaultValue = Constants.DEFAULT_PAGE_NUMBER) Integer pagina,
		@RequestParam(value = "limite", required = false, defaultValue = Constants.DEFAULT_PAGE_SIZE) Integer limite) {
		PagedResponse<UsuarioExterno> response = usuarioService.getAllUsuariosByFiltro(params, pagina, limite);
		
		return new ResponseEntity<PagedResponse<UsuarioExterno>>(response, HttpStatus.OK);
	}
	
	/**
     * GET /funcoes/: recupera todas os funções. 
     *
     * @return o Page com as Funcões no body.
     */
	@GetMapping("/funcoes")
	public Page<Funcao> getAllFuncoes(Pageable pageable) {
	    return funcaoRepository.findAll(pageable);
	}
	
	/**
     * POST /usuarios : Cria um novo UsuarioExterno.
     *
     * @param UsuarioExternoRequest
     *            o UsuarioExterno a ser criado
     * @return o ResponseEntity com status 201 (Created)e o body com status success true ou false se of usuarioExterno já existe com o CPF
     */
    /**
     * @param usuarioRequest
     * @return
     */
	@PostMapping("/usuarios")
	public ResponseEntity<ApiResponse> createUsuario(@Valid @RequestBody UsuarioExternoRequest usuarioRequest) {
		ApiResponse usuarioResponse = usuarioService.createUsuario(usuarioRequest);
		this.log.debug("REST request to save UsuarioExterno : {}", usuarioRequest.getNu_cpf());
		
		return new ResponseEntity<ApiResponse>(usuarioResponse, HttpStatus.CREATED);
	}
	
	/**
     * PATCH /usuarios/id : Atualiza a situação de um usuário existente.
     *
     * @param id do usuário para atualização
     * @return o ResponseEntity com status 200 (OK) e com o usuário atualizado no body, ou com status false
     *         se o cpf já estiverer em uso ou se a situação não for válida.
     */
	@PatchMapping("/usuarios/{id}")
	public ResponseEntity<ApiResponse> partialUpdateUsuario(@PathVariable(name = "id") Long id,
			@Valid @RequestBody UsuarioExternoRequest usuarioRequest) {
		ApiResponse usuarioResponse = usuarioService.partialUpdateUsuario(id, usuarioRequest);
		this.log.debug("REST request to update UsuarioExterno : {}", usuarioRequest.getNu_cpf());
		
		return new ResponseEntity<ApiResponse>(usuarioResponse, HttpStatus.OK);
	}
	
	/**
     * PUT /usuarios/id : Atualiza os dados de um usuário existente.
     *
     * @param id do usuário para atualização
     * @return o ResponseEntity com status 200 (OK) e com o usuário atualizado no body, ou com status false
     *         se o cpf já estiverer em uso.
     */
	@PutMapping("/usuarios/{id}")
	public ResponseEntity<ApiResponse> updateUsuario(@PathVariable(name = "id") Long id,
			@Valid @RequestBody UsuarioExternoRequest usuarioRequest) {
		ApiResponse usuarioResponse = usuarioService.updateUsuario(id, usuarioRequest);
		this.log.debug("REST request to update UsuarioExterno : {}", usuarioRequest.getNu_cpf());
		
		return new ResponseEntity<ApiResponse>(usuarioResponse, HttpStatus.OK);
	}
	
	 /**
     * DELETE /usuarios/id : deleta um determinado usuário.
     *
     * @param id
     *            o id do usuário a ser deletado
     * @return um ResponseEntity com status 200 (OK)
     */
	@DeleteMapping("/usuarios/{id}")
	public ResponseEntity<ApiResponse> deleteUsuario(@PathVariable(name = "id") Long id) {
		ApiResponse usuarioResponse = usuarioService.deleteUsuario(id);
		this.log.debug("REST request to delete UsuarioExterno : {}", id);
		
		return new ResponseEntity<ApiResponse>(usuarioResponse, HttpStatus.OK);
	}
	
}
