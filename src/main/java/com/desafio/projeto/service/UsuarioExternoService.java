package com.desafio.projeto.service;

import java.util.Map;

import javax.validation.Valid;


import com.desafio.projeto.config.ApiResponse;
import com.desafio.projeto.config.PagedResponse;
import com.desafio.projeto.config.UsuarioExternoRequest;
import com.desafio.projeto.model.UsuarioExterno;

/**
 * Service Interface para administrar UsuarioExterno.
 */
public interface UsuarioExternoService {
	/**
     * Buscar aits por filtro.
     *
     * @param params
	 * @param limite 
	 * @param pagina 
     * @param pageable
     * @return
     */
	PagedResponse<UsuarioExterno> getAllUsuariosByFiltro(Map<String, String> params, int pagina, int limite);
	
	/**
     * Atualiza toda a informação para um usuario específico, e retorna o usuário modificado.
     */
	ApiResponse updateUsuario(Long id,UsuarioExternoRequest usuarioRequest);
	
	/**
     * Deleta um determinado usuário.
     *
     * @param id, o id da entidade
     */
	ApiResponse deleteUsuario(Long id);
	
	/**
     * Cria um usuário.
     *
     * @param usuarioExterno
     *            the id of the entity
     * @return the entity
     */
	ApiResponse createUsuario(UsuarioExternoRequest usuarioRequest);
	
	/**
     * Atualiza as informações parciais de um usuario, e retorna o usuário modificado.
     */
	ApiResponse partialUpdateUsuario(Long id, @Valid UsuarioExternoRequest usuarioRequest);

}
