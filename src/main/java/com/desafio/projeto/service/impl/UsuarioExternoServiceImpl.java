package com.desafio.projeto.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.desafio.projeto.config.ApiResponse;
import com.desafio.projeto.config.PagedResponse;
import com.desafio.projeto.config.UsuarioExternoRequest;
import com.desafio.projeto.exceptions.ApiExceptions;
import com.desafio.projeto.model.Funcao;
import com.desafio.projeto.model.UsuarioExterno;
import com.desafio.projeto.repository.FuncaoRepository;
import com.desafio.projeto.repository.UsuarioExternoRepository;
import com.desafio.projeto.service.UsuarioExternoService;
import com.desafio.projeto.utils.AppUtils;
import com.desafio.specification.UsuarioExternoSpecifications;

import static com.desafio.projeto.utils.Constants.ID;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

/**
 * Service Implementação para administrar um UsuarioExterno.
 */
@Service
public class UsuarioExternoServiceImpl implements UsuarioExternoService{
	private static final String ERROR = "Erro ao deletar usuário externo";
	private static final String ERROR_UPDATE = "Usuário não existe";
	private static final String ERROR_CREATE = "Operação não realizada. Usuário já incluído.";
	private static final String ERROR_CONTAINS_SITUACAO = "Operação não realizada.Situação deve estar entre: A-Ativo ou I-Inativo";
	private static final String ERROR_CONTAINS_PERFIL = "Operação não realizada.Perfil deve estar entre: 1-Gestor Municipal, 2-Gestor Estadual, 3-Gestor Nacional";
	private static final String CADASTRO_OK = "Cadastro efetuado com sucesso!";
	private static final String ATUALIZACAO_OK = "Alteração efetuada com sucesso!";
	private static final String EXCLUSAO_OK = "Exclusão efetuada com sucesso.";
	
	private List<String> situacoes = Arrays.asList("A", "I");
	private List<Integer> perfis = Arrays.asList(1, 2, 3);
	
	@Autowired
	private UsuarioExternoRepository usuarioRepository;
	
	@Autowired
	private FuncaoRepository funcaoRepository;

	@Override
	public PagedResponse<UsuarioExterno> getAllUsuariosByFiltro(Map<String, String> params, int pagina, int limite) {
		Pageable pageable = PageRequest.of(pagina, limite, Sort.Direction.DESC, ID);
		
		String nome = params.get("nome");
		String situacao = params.get("situacao");
		String perfil = params.get("perfil");
	
		Specification<UsuarioExterno> specification = Specification
		    .where(nome == null ? null : UsuarioExternoSpecifications.nomeContem(nome))
		    .and(situacao == null ? null : UsuarioExternoSpecifications.situacao(situacao))
		    .and(perfil == null ? null : UsuarioExternoSpecifications.perfil(Integer.parseInt(perfil)));
		
		Page<UsuarioExterno> usuarios = usuarioRepository.findAll(specification, pageable);
	    List<UsuarioExterno> dados = usuarios.getNumberOfElements() == 0 ? Collections.emptyList() : usuarios.getContent();
	      
	    return new PagedResponse<UsuarioExterno>(dados, usuarios.getNumber(), usuarios.getSize(), usuarios.getTotalElements(), usuarios.getTotalPages(), usuarios.isLast(), usuarios.isFirst());
	}

	@SuppressWarnings("null")
	@Override
	public ApiResponse updateUsuario(Long id, UsuarioExternoRequest usuarioRequest) {
		UsuarioExterno usuario = usuarioRepository.findById(id).get();
		UsuarioExterno usuarioExistente = usuarioRepository.findByCpf(usuarioRequest.getNu_cpf().replaceAll("[.-]", ""));
		Funcao funcao = funcaoRepository.findById(usuarioRequest.getCo_funcao()).get();
		
		if (usuarioRequest != null) {
			if(usuarioRequest.getNu_cpf() != null) {
				if(usuarioExistente != null && usuario.getId() == id)
					usuario.setNu_cpf(usuario.getNu_cpf().replaceAll("[.-]", ""));
				else
					return new ApiResponse(Boolean.FALSE, ERROR_CREATE);
			}
			if(usuarioRequest.getNo_usuario() != null)
				usuario.setNo_usuario(usuarioRequest.getNo_usuario());
			if(usuarioRequest.getDe_email() != null)
				usuario.setDe_email(usuarioRequest.getDe_email());
			if(usuarioRequest.getIc_situacao() != null) {
				if(situacoes.contains(usuarioRequest.getIc_situacao()))
					usuario.setIc_situacao(usuarioRequest.getIc_situacao().toUpperCase());
				else
					return new ApiResponse(Boolean.FALSE, ERROR_CONTAINS_SITUACAO);
			}
			if(usuarioRequest.getIc_perfil_acesso() != 0) {
				if(perfis.contains(usuarioRequest.getIc_perfil_acesso()))
					usuario.setIc_perfil_acesso(usuarioRequest.getIc_perfil_acesso());
				else
					return new ApiResponse(Boolean.FALSE, ERROR_CONTAINS_PERFIL);
			}
			if(usuarioRequest.getCo_funcao() != null) {
				if(funcao != null)
					usuario.setCo_funcao(funcao);
			}
			if(usuarioRequest.getNu_telefone() != null)
				usuario.setNu_telefone(usuarioRequest.getNu_telefone());
				
			usuarioRepository.save(usuario);
			
			return new ApiResponse(Boolean.TRUE, ATUALIZACAO_OK);
		}
		return new ApiResponse(Boolean.FALSE, ERROR_UPDATE);
	}

	@Override
	public ApiResponse deleteUsuario(Long id) {
		UsuarioExterno usuario = usuarioRepository.findById(id).get();

		if (usuario != null) {
			usuarioRepository.deleteById(usuario.getId());
			return new ApiResponse(Boolean.TRUE, EXCLUSAO_OK);
		}

		throw new ApiExceptions(HttpStatus.BAD_REQUEST, ERROR);
	}

	@Override
	public ApiResponse createUsuario(UsuarioExternoRequest usuarioRequest) {
		UsuarioExterno usuarioExistente = usuarioRepository.findByCpf(usuarioRequest.getNu_cpf().replaceAll("[.-]", ""));
		Funcao funcao = funcaoRepository.findById(usuarioRequest.getCo_funcao()).get();
		
		if(usuarioExistente != null)
			return new ApiResponse(Boolean.FALSE, ERROR_CREATE);
				
		UsuarioExterno usuario = new UsuarioExterno();
		usuario.setNu_cpf(usuarioRequest.getNu_cpf().replaceAll("[.-]", ""));
		usuario.setNo_usuario(usuarioRequest.getNo_usuario());
		usuario.setDe_email(usuarioRequest.getDe_email());
		if(situacoes.contains(usuarioRequest.getIc_situacao()))
			usuario.setIc_situacao(usuarioRequest.getIc_situacao().toUpperCase());
		else
			return new ApiResponse(Boolean.FALSE, ERROR_CONTAINS_SITUACAO);
		if(perfis.contains(usuarioRequest.getIc_perfil_acesso()))
			usuario.setIc_perfil_acesso(usuarioRequest.getIc_perfil_acesso());
		else
			return new ApiResponse(Boolean.FALSE, ERROR_CONTAINS_PERFIL);
		usuario.setCo_funcao(funcao);
		usuario.setNu_telefone(usuarioRequest.getNu_telefone());
		
		usuarioRepository.save(usuario);
		
		return new ApiResponse(Boolean.TRUE,CADASTRO_OK);
	}

	@Override
	public ApiResponse partialUpdateUsuario(Long id, @Valid UsuarioExternoRequest usuarioRequest) {
		UsuarioExterno usuario = usuarioRepository.findById(id).get();
		
		if(usuario == null)
			return new ApiResponse(Boolean.FALSE, ERROR_UPDATE);
		
		if(usuarioRequest.getIc_situacao() != null) {
			if(situacoes.contains(usuarioRequest.getIc_situacao())) {
				usuario.setIc_situacao(usuarioRequest.getIc_situacao().toUpperCase());
				usuarioRepository.save(usuario);
				
				return new ApiResponse(Boolean.TRUE, ATUALIZACAO_OK);

			}
			else
				return new ApiResponse(Boolean.FALSE,  ERROR_CONTAINS_SITUACAO);
		}
		else
			return new ApiResponse(Boolean.FALSE,  ERROR_CONTAINS_SITUACAO);
	}
	
}
