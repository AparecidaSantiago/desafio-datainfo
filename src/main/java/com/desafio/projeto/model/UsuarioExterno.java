package com.desafio.projeto.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "usuario_externo")
public class UsuarioExterno {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
	@NotNull(message = "CPF não pode ser nulo")
    @Column(name = "nu_cpf", length = 11)
    private String nu_cpf;
	
	@NotNull(message = "Nome do usuário não pode ser nulo")
    @Column(name = "no_usuario", length = 60)
    private String no_usuario;
	
	@NotNull(message = "Email não pode ser nulo")
    @Column(name = "de_email", length = 255)
    private String de_email;
	
	@NotNull(message = "Situação não pode ser nula")
    @Column(name = "ic_situacao", length = 1)
    private String ic_situacao;
	
	@NotNull(message = "Perfil do usuário não pode ser nulo")
	@Column(name = "ic_perfil_acesso")
    private int ic_perfil_acesso;
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@OneToOne
    private Funcao co_funcao;
	
    @Column(name = "nu_telefone", length = 11)
    private String nu_telefone;
}
