package com.desafio.projeto.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "funcao_usuario_externo")
public class Funcao {
	@Id
	@Column(name = "co_funcao")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long co_funcao;
	
	@NotBlank
    @Column(name = "no_funcao", length = 50)
    private String nome;
}
