# Aplicação
Este projeto implementa um sistema para administração de usuários. A aplicação consiste em um projeto Java com Spring e outras tecnologias. 

## Requisitos de instalação
- JRE e JDK versão 1.8
- Lombok 1.18.12+ (Dowload lombok: https://projectlombok.org/download)
- Eclipse IDE Java
- PostgreSQL
- NodeJS LTS
- NPM LTS
- Spring Tools Suite (STS) 4 for Eclipse
- Apache Tomcat 8.5
- JUnit 4
- Angular 10

- Baixe o lombok e o eclipse através dos endereços abaixo:
- Lombok: https://projectlombok.org/downloads/lombok.jar
- Eclipse: https://www.eclipse.org/downloads/download.php?file=/technology/epp/downloads/release/2019-12/R/eclipse-jee-2019-12-R-linux-gtk-x86_64.tar.gz

- Extraia o eclipse para um diretório;
- Abra o terminal e acesse o diretório onde foi baixado o lombok.jar e execute o comando abaixo. Na janela aberta navegue até o diretório onde está o eclipse e selecione o arquivo "eclipse.ini"
```shell
sudo java -jar lombok.jar 
```

- Execute o Eclipse, no menu selecione Help e depois Eclipse Marketplace
- Na janela aberta, pesquise por "Spring Tools 4" e instale;
- Depois de instalado, no menu selecione Window/Perspective/Open Perspective/Other/Spring;
- Vá no menu **Run** > **Run Configurations** > Crie um novo Spring Boot App > na Spring Boot selecione o projeto 'desafio-datainfo'
- Importe o projeto. No Eclipse, na aba **File > Import**. Selecione **Maven > Existing Maven Projects**. Insira o caminho onde se encontra a pasta do projeto 'desafio-datainfo' e clique em “Finish”. O
projeto será importado e todas as bibliotecas serão baixadas automaticamente. Aguarde até que o processo todo termine.
- Na raiz do projeto execute: 
```shell 
npm install -g @angular/cli 
```
- Procure a aba “Boot Dashboard”, selecione o projeto 'desafio-datainfo' e clique no ícone de start ou debug. Pronto! A URL para acesso é **localhost:8080**.

## Banco de Dados
- Execute o seguinte script no Banco de Dados:
```shell
INSERT INTO funcao_usuario_externo(co_funcao, no_funcao) VALUES (1, 'Gestor');
INSERT INTO funcao_usuario_externo(co_funcao, no_funcao) VALUES (2, 'Administrador');
INSERT INTO funcao_usuario_externo(co_funcao, no_funcao) VALUES (3, 'Frente de Caixa');
```

# Testes
- Para executar os testes, no eclipse, clique com o botão direto em cima da classe DesafiodatainfoApplicationTests.java e escolha a opção Run/RunAs > JUnit Test.